/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package r_tree;

/**
 *
 * @author Robo
 */
public class Constants {

    /**
     *
     */
    public static final int PAGE_INDEX_SIZE = 1024;//128;//1024;

    /**
     * A two dimenslonal rectangle can be represented by four numbers of four
     * bytes each If a pointer also takes four bytes, each entry requires 20
     * bytes 
     */
    public static final int ENTRIES = 20;

    /**
     * A page of 1024 bytes will hold 50 ENTRIES + header
     */
    public static final int M = 50;//4;//50
    
    /**
     * m <= M/2
     */
    public static final int m = 20;//2;//20
}
