/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package r_tree;

import java.awt.Rectangle;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Robert
 */
public class Node {

    private Rectangle _mbr;
    private int _parentAddress;
    private LinkedList<Node> _children;
    private boolean _leaf;
    private int _address;
    private Data _data;
    
    public Node() {
        this._children = new LinkedList();
    }
    
    public Node(Rectangle mbr, Data data) {
        this._children = new LinkedList();
        this._mbr = mbr;
        this._data = data;
    }
    
    public Node(Rectangle mbr, int address) {
        this._mbr = mbr;
        this._address = address;
    }
    
    public Node(Rectangle mbr, boolean leaf, int address) {
        this._mbr = mbr;
        this._leaf = leaf;
        this._children = new LinkedList();
        this._address = address;
    }
    
    /**
     * @return the _leaf
     */
    public boolean leaf() {
        return _leaf;
    }

    /**
     * @return the _children
     */
    public LinkedList<Node> children() {
        return _children;
    }

    /**
     * @return the _mbr
     */
    public Rectangle mbr() {
        return _mbr;
    }


    /**
     * @param _leaf the _leaf to set
     */
    public void setLeaf(boolean _leaf) {
        this._leaf = _leaf;
    }
    public void setMbr() {
        int minX=Integer.MAX_VALUE,minY=Integer.MAX_VALUE,
            maxX=Integer.MIN_VALUE,maxY=Integer.MIN_VALUE;
        
        for (Node node : _children) {
            if (node != null) {
                minX = (int) Math.min(minX, node.mbr().getX());
                minY = (int) Math.min(minY, node.mbr().getY());
                maxX = (int) Math.max(maxX, node.mbr().getX() + node.mbr().getWidth());
                maxY = (int) Math.max(maxY, node.mbr().getY() + node.mbr().getHeight());
            }
        }
        
        //System.out.println((maxX-minX));
        this._mbr = new Rectangle(minX,minY,(maxX-minX),(maxY-minY));
            if(_mbr.getHeight() == 835) {
                //System.out.println();
            }
    }

    public void add(Node E) {
        this._children.add(E);
        E.setParentAddress(_address);// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!pridane
        setMbr();
    }
    
    public boolean intersects(Rectangle r) {
        double tw = this._mbr.getWidth();
        double th = this._mbr.getHeight();
        double rw = r.getWidth();
        double rh = r.getHeight();
        double tx = this._mbr.getX();
        double ty = this._mbr.getY();
        double rx = r.getX();
        double ry = r.getY();
        rw += rx;
        rh += ry;
        tw += tx;
        th += ty;
        //      overflow || intersect
        return ((rw < rx || rw >= tx) &&
                (rh < ry || rh >= ty) &&
                (tw < tx || tw >= rx) &&
                (th < ty || th >= ry));
    }
    
    public byte[] getByteArray()  {     
        ByteArrayOutputStream hlpByteArrayOutputStream= new ByteArrayOutputStream();
        DataOutputStream hlpOutStream = new DataOutputStream(hlpByteArrayOutputStream);
        byte[] result = new byte[Constants.PAGE_INDEX_SIZE];
        try{
            hlpOutStream.writeBoolean(this._leaf);
            hlpOutStream.writeShort(_children.size());
            hlpOutStream.writeInt(parentAddress());
            hlpOutStream.writeInt(this._address);
            byte[] header = hlpByteArrayOutputStream.toByteArray();
            System.arraycopy(header, 0, result, 0, header.length);
            int i = 0;
            for(Node c: _children) {
                byte[] record = c.toByteArray();
                System.arraycopy(record, 0, result, header.length + i*record.length, record.length);
                i++;
            }
        }catch (IOException e){
            throw new IllegalStateException("Error during conversion to byte array.");
        }
        return result;
    }

    private byte[] toByteArray() {
        ByteArrayOutputStream hlpByteArrayOutputStream= new ByteArrayOutputStream();
        DataOutputStream hlpOutStream = new DataOutputStream(hlpByteArrayOutputStream);
        try {
            hlpOutStream.writeInt(_address);
            hlpOutStream.writeInt((int)_mbr.getX());
            hlpOutStream.writeInt((int)_mbr.getY());
            hlpOutStream.writeInt((int)(_mbr.getX() + _mbr.getWidth()));
            hlpOutStream.writeInt((int)(_mbr.getY() + _mbr.getHeight()));
            //System.out.println(_mbr.getX()+", "+_mbr.getWidth()+", "+(int)(_mbr.getX() + _mbr.getWidth()) + ", "+_mbr.getY()+", "+_mbr.getHeight()+", "+(int)(_mbr.getY() + _mbr.getHeight()));
        } catch (IOException ex) {
            Logger.getLogger(Node.class.getName()).log(Level.SEVERE, null, ex);
        }
        return hlpByteArrayOutputStream.toByteArray();
    }
    
    public int length() {
        return Constants.M*Constants.ENTRIES + 1/*leaf*/ + 2/*size*/ + 4/*parent*/ +4 /*address*/;
    }

    /**
     * @param _address the _address to set
     */
    public void setAddress(int _address) {
        this._address = _address;
    }

    public void fromByteArray(byte[] byteRoot) {
        ByteArrayInputStream hlpByteArrayInputStream = new ByteArrayInputStream(byteRoot);
        DataInputStream hlpInStream = new DataInputStream(hlpByteArrayInputStream);
        int size;
        try {
            this._leaf = hlpInStream.readBoolean();
            size = hlpInStream.readShort();
            this._parentAddress = hlpInStream.readInt();
            this._address = hlpInStream.readInt();
        } catch (IOException e) {
            throw new IllegalStateException("Error during conversion from byte array.");
        }
        byte[] byteChild = new byte[Constants.ENTRIES];
        for(int i=0;i<size;i++) {
            System.arraycopy(byteRoot, 1/*leaf*/ + 2/*size*/ + 4/*parent*/ +4 /*address*/ + i*Constants.ENTRIES, byteChild, 0, Constants.ENTRIES);
            Node child = childFromByteArray(byteChild);
            add(child);
        }
    }

    private Node childFromByteArray(byte[] byteChild) {
        ByteArrayInputStream hlpByteArrayInputStream = new ByteArrayInputStream(byteChild);
        DataInputStream hlpInStream = new DataInputStream(hlpByteArrayInputStream);
        try {
            int address = hlpInStream.readInt();
            int leftX = (int)hlpInStream.readInt();
            int topY = (int)hlpInStream.readInt();
            int rightX = (int)hlpInStream.readInt();
            int botY = (int)hlpInStream.readInt();
            Node n = new Node(new Rectangle(leftX, topY, rightX-leftX, botY-topY),address);   

            return n;
        } catch (IOException e) {
            throw new IllegalStateException("Error during conversion from byte array.");
        }
    }

    /**
     * @return the _address
     */
    public int address() {
        return _address;
    }

    /**
     * @return the _parentAddress
     */
    public int parentAddress() {
        return _parentAddress;
    }

    /**
     * @param _parentAddress the _parentAddress to set
     */
    public void setParentAddress(int _parentAddress) {
        this._parentAddress = _parentAddress;
    }
    
    public void setMbr(Rectangle mbr) {
        this._mbr = mbr;
    }

    /**
     * @return the data
     */
    public Data data() {
        return _data;
    }

    /**
     * @param _data the _data to set
     */
    public void setData(Data _data) {
        this._data = _data;
    }
    
    

}
