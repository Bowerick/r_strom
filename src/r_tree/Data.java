/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package r_tree;

import java.io.DataInputStream;
import java.io.IOException;

/**
 *
 * @author Robo
 */
public abstract class Data {
    
    private static final char SPECIAL_CHAR = '*';
    
    public abstract Data fromByteArray(byte[] paArray);

    public abstract byte[] toByteArray();

    public abstract int length();
   
   
    public String getFullString(String src,int MAX_LENGTH) {
        if(src.length() < MAX_LENGTH) {
            String pom = "";
            for(int i=0;i<MAX_LENGTH;i++) {
                if(i<src.length()) {
                    pom += src.charAt(i);
                } else
                    pom += SPECIAL_CHAR;
            }
            return pom;
        } else {
            if(src.length() == MAX_LENGTH) {
                return src;
            } else {
                return src.substring(0, MAX_LENGTH);
            }
        }
    }
    
    public String getString(DataInputStream hlpInStream, int MAX_LENGTH) {
        String word= "";
        char c;
        for(int i=0;i<MAX_LENGTH;i++) {
            try {
                c = hlpInStream.readChar();
                if(c != SPECIAL_CHAR)
                    word += c;
            } catch (IOException ex) {
            }
        }
        return word;
    }
}
