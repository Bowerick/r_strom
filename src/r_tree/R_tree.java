/*
 * To _change this license header, choose License Headers in Project Properties.
 * To _change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package r_tree;

import java.awt.Rectangle;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Robo
 */
public class R_tree {
    
    private Node _root;
    private boolean _change;
    private RandomAccessFile _rafIndex;
    private RandomAccessFile _rafData;
    private static final String INDEX = "index.bin";
    private static final String DATA = "data.bin";
    private int _indexAddress;
    private int _dataAddress;
    private Data _data;
    private String _directory;
    
    public R_tree(Data data, String directoryName) {
        _change = true;
        _indexAddress = 0;
        _dataAddress = 0;
        _root = null;
        _data = data;
        _directory = directoryName;
        openFile();
        
        try {
            Node n = new Node();
            if(n.length() > Constants.PAGE_INDEX_SIZE) {
                throw new PageSizeException("Page size of index file("+Constants.PAGE_INDEX_SIZE+") is too small for node("+n.length()+").");
            }
        } catch (PageSizeException ex) {
            Logger.getLogger(R_tree.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void init() {
        writeRootAddressToFile(-1);
        _indexAddress += 4;
    }
    
    public LinkedList<Node> search(Rectangle S) {
        LinkedList<Node> result = new LinkedList();
        int address = loadRootAddress();
        if(address == -1) {
            return null;
        }
        int pom =1;
        search(S,address,result,pom);
        return result;
        
    }
    
    private void search(Rectangle S, int addressT, LinkedList<Node> result,int pom) {
        Node T = new Node();
        byte[] byteT = loadNodeFromFile(addressT);
        T.fromByteArray(byteT);
        LinkedList<Node> children = T.children();
        if(!T.leaf()) {
            for(Node c: children) {
                if(c.intersects(S)) {
                    pom++;
                    search(S,c.address(),result,pom);
                    pom--;
                }
            }
        } else {
           for(Node c: children) {
                if(c.intersects(S)) {
                    byte[] byteData = loadDataFromFile(c.address());
                    c.setData(_data.fromByteArray(byteData));
                    result.add(c);
                }
            } 
        }
    }
    
    public void insert(Node E) {
        E.setLeaf(true);
        E.setAddress(_dataAddress);
        _dataAddress += _data.length();
        int address = loadRootAddress();
        if(address == -1) {
            _root = new Node(E.mbr(),true,_indexAddress);
            _indexAddress += Constants.PAGE_INDEX_SIZE;
            _root.add(E);
            writeNodeToFile(_root);
            writeDataToFile(E);
            writeRootAddressToFile(_root.address());
            return;
        } else {
            _root = new Node();
            byte[] byteRoot = loadNodeFromFile(address);
            _root.fromByteArray(byteRoot);
        }
        Node L = chooseLeaf(E);
        if(L.children().size()+1 > Constants.M) {
            Node[] split = quadraticSplit(L,E);
            adjustTree(split[0],split[1]);
        } else {
            L.add(E);
            adjustTree(L,null);
        }
        writeDataToFile(E);
    }
    
    private Node chooseLeaf(Node E) {
        Node N = _root;
        LinkedList<Node> children;
        while(!N.leaf()) {
            children = N.children();
            Rectangle rect;
            double area;
            double leastEnlargement = Double.POSITIVE_INFINITY;
            //entry in N whose rectangle needs least enlargement to include E
            for(Node c: children) {
                rect = (Rectangle) c.mbr().createUnion(E.mbr());
                area = (rect.getWidth()*rect.getHeight())  -  (c.mbr().getWidth()*c.mbr().getHeight());
                if(area < leastEnlargement) {
                    leastEnlargement = area;
                    N = c;
                }
                //Resolve ties by choosing the entry with the rectangle of smallest area
                if(area == leastEnlargement) {
                    double area1 = N.mbr().getWidth()*N.mbr().getHeight();
                    double area2 = c.mbr().getWidth()*c.mbr().getHeight();
                    if(area2 < area1) {
                        N = c;
                    }
                }
            }
            byte[] byteRoot = loadNodeFromFile(N.address());
            Node n = new Node();
            n.fromByteArray(byteRoot);
            N = n;
        }
        return N;
    }

    private Node[] quadraticSplit(Node L, Node E) {
        Node[] group = new Node[2];
        Node[] seed = pickSeeds(L,E);
        
        LinkedList notAssigned = new LinkedList();
        notAssigned.addAll(L.children());
        
        notAssigned.remove(seed[0]);
        notAssigned.remove(seed[1]);
        group[0] = L;
        group[0].children().clear();
        group[0].add(seed[0]);
        
        group[1] = new Node(seed[1].mbr(),true,_indexAddress);
        _indexAddress += Constants.PAGE_INDEX_SIZE;
        group[1].add(seed[1]);
        group[1].setParentAddress(L.parentAddress());
        
        while(!notAssigned.isEmpty()) {
            //If one group has so few ENTRIES that all the rest must be assigned
            //to it in order for it to have the minimum number m , assign them 
            //and stop
            if((group[0].children().size() + notAssigned.size()) == Constants.m) {
                group[0].children().addAll(notAssigned);
                for(Node c:group[0].children()) {
                    c.setParentAddress(group[0].address());
                }
                group[0].setMbr();
                return group;
            }
            if((group[1].children().size() + notAssigned.size()) == Constants.m) {
                group[1].children().addAll(notAssigned);
                for(Node c:group[1].children()) {
                    c.setParentAddress(group[1].address());
                }
                group[1].setMbr();
                return group;
            }
            
            pickNext(notAssigned,group);
            
        }
        
        return group;
        
    }

    private Node[] pickSeeds(Node L, Node E) {
        Node[] best = new Node[2];
        LinkedList<Node> children = L.children();
        children.add(E);
        Rectangle combinedRect;
        double maxWastedArea = Double.NEGATIVE_INFINITY;
        double wastedArea;
        for(Node n: children) {
            for(Node n2: children) {
                if(n != n2) {
                    combinedRect = (Rectangle) n.mbr().createUnion(n2.mbr());
                    wastedArea = (combinedRect.getWidth()*combinedRect.getHeight());
                    wastedArea -= n.mbr().getWidth() * n.mbr().getHeight();
                    wastedArea -= n2.mbr().getWidth() * n2.mbr().getHeight();
                    if(wastedArea > maxWastedArea) {
                        best[0] = n;
                        best[1] = n2;
                        maxWastedArea = wastedArea;
                    }
                }
            }
        }
        return best;
    }

    /*  Determine cost of putting each entry in each group
     Be G1 and G2 the two new groups to which the ENTRIES are to be assigned. 
    For each entry E not yet in a group, compose a rectangle r0 including G0, E
    and rectangle r1 including G1, E 
    Choose any entry with the maximum difference between area0 and area1
    */
    private void pickNext(LinkedList<Node> notAssigned, Node[] group) {
        Node next = null;
        Rectangle r0;
        Rectangle r1;
        int enlargedLeast = -1;
        double maxDifference = Double.NEGATIVE_INFINITY;
        for(Node n: notAssigned) {
            r0 = (Rectangle)group[0].mbr().createUnion(n.mbr());
            r1 = (Rectangle)group[1].mbr().createUnion(n.mbr());
            double area0 = r0.getWidth()*r0.getHeight();
            area0 -= group[0].mbr().getWidth()*group[0].mbr().getHeight();
            double area1 = r1.getWidth()*r1.getHeight();
            area1 -= group[1].mbr().getWidth()*group[1].mbr().getHeight();
            double difference = Math.abs(area0-area1);
            if(difference > maxDifference) {
                next = n;
                //Add it to the group whose covering rectangle will have to be
                //enlarged least to accommodate it.
                if(area1 > area0) {
                    enlargedLeast = 0;
                }
                else if(area0 > area1) {
                    enlargedLeast = 1;
                }
                else if(area0 == area1) {
                    enlargedLeast = 2;
                }
                maxDifference = difference;
            }
        }
        //Resolve ties by adding the entry to the group with smaller area, then
        //to the one with fewer ENTRIES, then to either
        if(enlargedLeast == 2) {
            if(_change) {
                //smaller area
                double area0 = group[0].mbr().getWidth()*group[0].mbr().getHeight();
                double area1 = group[1].mbr().getWidth()*group[1].mbr().getHeight();
                if(area0 < area1)
                    group[0].add(next);
                else
                    group[1].add(next);
                _change = false;
            } else {
                //fewer ENTRIES
                if(group[0].children().size() < group[1].children().size())
                    group[0].add(next);
                else
                    group[1].add(next);
                _change = true;
            }
        } else {
            group[enlargedLeast].add(next);
        }
        notAssigned.remove(next);
    }

    private void adjustTree(Node N, Node NN) {
        while(true) {
            if(N.address() == _root.address()) {
                if(NN != null) {
                    _root = new Node(N.mbr(),false,_indexAddress);
                    _indexAddress += Constants.PAGE_INDEX_SIZE;
                    writeRootAddressToFile(_root.address());
                    
                    N.setParentAddress(_root.address());
                    NN.setParentAddress(_root.address());
                    _root.add(N);
                    _root.add(NN);
                    
                    writeNodeToFile(_root);
                    writeNodeToFile(N);
                    writeNodeToFile(NN);
                    if(!NN.leaf()) {
                        for(Node c: NN.children()) {
                            Node n = new Node();
                            n.fromByteArray(loadNodeFromFile(c.address()));
                            n.setParentAddress(c.parentAddress());
                            writeNodeToFile(n);
                        }
                    }
                } else {
                    writeNodeToFile(N);
                }
                return;
            }
            // Let P be the parent node of N , and let c be N’s entry in P .
            //Adjust c so that it tightly encloses all entry rectangles in N . 
            Node P = new Node();
            byte[] byteP = loadNodeFromFile(N.parentAddress());
            P.fromByteArray(byteP);          
            
            for(Node c: P.children()) {
                if(c.address() == N.address()) {
                    c.setMbr(N.mbr());
                    P.setMbr();
                    break;
                }
            }
            
            writeNodeToFile(P);
            
            if(NN != null) {
                if(P.children().size()+1 > Constants.M) {
                    writeNodeToFile(N);
                    Node[] split = quadraticSplit(P,NN);
                    N = split[0];
                    N.setLeaf(false);
                    writeNodeToFile(NN);
                    if(!NN.leaf()) {
                        for(Node c: NN.children()) {
                            Node n = new Node();
                            n.fromByteArray(loadNodeFromFile(c.address()));
                            n.setParentAddress(c.parentAddress());
                            writeNodeToFile(n);
                        }
                    }
                    NN = split[1];
                    NN.setLeaf(false);
                } else {
                    P.add(NN);
                    if(!NN.leaf()) {
                        for(Node c: NN.children()) {
                            Node n = new Node();
                            n.fromByteArray(loadNodeFromFile(c.address()));
                            n.setParentAddress(c.parentAddress());
                            writeNodeToFile(n);
                        }
                    }
                    
                    writeNodeToFile(P);
                    writeNodeToFile(N);
                    writeNodeToFile(NN);
                    N = P;
                    NN = null;
                }
            } else {
                writeNodeToFile(N);
                N = P;
            }
        }
    }
    
    public byte[] loadNodeFromFile(int adresa) {
        byte[] result = new byte[Constants.PAGE_INDEX_SIZE];
        try {
            _rafIndex.seek(adresa);
            _rafIndex.readFully(result);
        } catch (IOException ex) { 
            Logger.getLogger(R_tree.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }
    
    public byte[] loadDataFromFile(int adresa) {
        byte[] result = new byte[_data.length()];
        try {
            _rafData.seek(adresa);
            _rafData.readFully(result);
        } catch (IOException ex) { 
            Logger.getLogger(R_tree.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }
    
    public void writeNodeToFile(Node n) {
        try {
            _rafIndex.seek(n.address());
            byte[] bytes = n.getByteArray();
            _rafIndex.write(bytes);
        } catch (IOException ex) {
            Logger.getLogger(R_tree.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void writeDataToFile(Node E) {
        try {
            _rafData.seek(E.address());
            byte[] bytes = E.data().toByteArray();
            _rafData.write(bytes);
        } catch (IOException ex) {
            Logger.getLogger(R_tree.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public int loadRootAddress() {
        int address;
        byte[] result = new byte[4];
        try {
            _rafIndex.seek(0);
            _rafIndex.readFully(result);
        } catch (IOException ex) {
            Logger.getLogger(R_tree.class.getName()).log(Level.SEVERE, null, ex);
        }
        ByteArrayInputStream hlpByteArrayInputStream = new ByteArrayInputStream(result);
        DataInputStream hlpInStream = new DataInputStream(hlpByteArrayInputStream);
        try {
            address = hlpInStream.readInt();
        } catch (IOException e) {
            throw new IllegalStateException("Error during conversion from byte array.");
        }
        return address;
    }

    private void writeRootAddressToFile(int address) {
        try {
            ByteArrayOutputStream hlpByteArrayOutputStream= new ByteArrayOutputStream();
            DataOutputStream hlpOutStream = new DataOutputStream(hlpByteArrayOutputStream);
            hlpOutStream.writeInt(address);
            byte[] result = hlpByteArrayOutputStream.toByteArray();
            _rafIndex.seek(0);
            _rafIndex.write(result);
        } catch (IOException ex) {
            Logger.getLogger(R_tree.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    public void openFile() {
        try {
            File dir = new File(_directory);
            dir.mkdir();
            _rafIndex = new RandomAccessFile(_directory+"/"+INDEX, "rw");
            _rafData = new RandomAccessFile(_directory+"/"+DATA, "rw");
        } catch (FileNotFoundException ex) {
            Logger.getLogger(R_tree.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void closeFile() {
        try {
            _rafIndex.close();
            _rafData.close();
        } catch (FileNotFoundException ex) {
            
        }   catch (IOException ex) {
            Logger.getLogger(R_tree.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public class PageSizeException extends Exception {
    
        public PageSizeException(String message) {
            super(message);
        }
    }
}
